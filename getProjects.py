from envparse import Env
import gitlab
import json
import os
import requests

env = Env()
if os.path.isfile('.env'):
    env.read_envfile('.env')

def main():
    personal_token = env('PERSONAL_TOKEN')
    group_number = env('GROUP_NUMBER')
    gitlab_host = env('GITLAB_HOST')

    gl = gitlab.Gitlab(gitlab_host, private_token=personal_token)
    gl.auth()
    projects = gl.projects.list(all=True)
    group = gl.groups.get(group_number)
    projects = group.projects.list(all=True)
    authorization = 'Bearer ' +  personal_token
    my_headers = {'Authorization': authorization}

    html_template = '''
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Dependencies List</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.21.1/dist/bootstrap-table.min.css">
  </head>
  <body>
    <table id="table">
  <thead>
    <tr>
      <th data-field="project">Project ID</th>
      <th data-field="name">Name</th>
      <th data-field="version">Version</th>
      <th data-field="package_manager">Package Manager</th>
      <th data-field="dependency_file_path">Dependency File Path</th>
      <th data-field="vulnerabilities">Vulnerabilities</th>
    </tr>
  </thead>
</table>

    <script src="https://cdn.jsdelivr.net/npm/jquery/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/bootstrap-table@1.21.1/dist/bootstrap-table.min.js"></script>

<script>
  var $table = $('#table')

  $(function() {
    var data =
    '''

    html_footer = '''

    $table.bootstrapTable({data: data})
  })
</script>
  </body>
</html>
    '''
    
    # connects to dependency API for each project
    dependency_list = []
    for project in projects:
        response = requests.get("https://gitlab.com/api/v4/projects/" + str(project.id) + "/dependencies", headers=my_headers)
        project_dependencies = response.json()
        for dependencies in project_dependencies: 
            dependencies["project"] = project.id
        # write to file
        dependency_list.extend(project_dependencies)

    big_string = html_template + json.dumps(dependency_list) + html_footer

    with open ("public/index.html", "w") as f:
        f.write(big_string)

    with open ("dependencies.json", "w") as f:
        f.write(json.dumps(dependency_list, indent=4))

if __name__ == '__main__':
    main()
